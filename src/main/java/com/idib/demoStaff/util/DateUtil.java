package com.idib.demoStaff.util;

import java.sql.Date;
import java.util.Calendar;

import static java.util.Calendar.*;

public class DateUtil {


    public static Date toSql(java.util.Date utilDate) {
        return new Date(utilDate.getTime());
    }

    public static java.util.Date toUtil(Date sqlDate) {
        return new java.util.Date(sqlDate.getTime());
    }

    public static boolean isWeekend(java.util.Date utilDate) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(utilDate);
        int dayOfWeek = c1.get(DAY_OF_WEEK);
        return dayOfWeek == SATURDAY || dayOfWeek == SUNDAY;
    }

    public static boolean isWeekend(Date sqlDate){
        return isWeekend(toUtil(sqlDate));
    }
}
